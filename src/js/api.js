import axios from "axios"

export default {
  async fetchMovies() {
    return axios.get("https://swapi.co/api/films/").then(response => response.data.results);
  }
}
